#!/usr/bin/env bash

set -e

containerSetup() {
  HOST_IP=${HOST_IP:-NONE}
  HOST_NETWORK=${HOST_NETWORK:-NONE}

  DNS_BACKEND=${DNS_BACKEND:-BIND9_DLZ}
  DNS_FORWARDER=${DNS_FORWARDER:-9.9.9.9}
  INTERFACES=${INTERFACES:-NONE}

  DOMAIN=${DOMAIN:-NONE}
  LDOMAIN=${DOMAIN,,}
  UDOMAIN=${DOMAIN^^}
  URDOMAIN=${UDOMAIN%%.*}

  JOIN=${JOIN:-false}
  JOINSITE=${JOINSITE:-NONE}

  ADMIN_USER=${ADMIN_USER:-administrator}
  ADMIN_PASS=${ADMIN_PASS:-YouShouldReallySetAPassword^123}

  NOCOMPLEXITY=${NOCOMPLEXITY:-false}
  INSECURELDAP=${INSECURELDAP:-false}
  USEOWNCERTS=${USEOWNCERTS:-false}

  if [[ ${HOST_IP} == "NONE" ]]; then
    printf "The environment variable HOST_IP is required!\nExample: HOST_IP=192.168.0.1\n"
    exit 255
  fi

  if [[ ${HOST_NETWORK} == "NONE" ]]; then
    printf "The environment variable HOST_NETWORK is required!\nExample: HOST_NETWORK=192.168.0.0/24\n"
    exit 255
  fi

  if [[ ${DOMAIN} == "NONE" ]]; then
    printf "The environment variable DOMAIN is required!\nExample: DOMAIN=example.local\n"
    exit 255
  fi

  if [[ ! -d "/etc/external" ]]; then
    printf "The directory /etc/external is required!\n"
    exit 255
  fi

  if [[ ! -w "/etc/external" ]]; then
    printf "The directory /etc/external must be writable!\n"
    exit 255
  fi

  # setup samba
  if [[ -f "/etc/external/smb.conf" ]]; then
    cp -f /etc/external/smb.conf /etc/samba/smb.conf
  else
    rm -f /etc/samba/smb.conf
    if [[ ${JOIN,,} == "true" ]]; then
      if [[ ${JOINSITE} == "NONE" ]]; then
        samba-tool domain join ${LDOMAIN} DC -U"${URDOMAIN}\\${ADMIN_USER}" --password="${ADMIN_PASS}" --dns-backend="${DNS_BACKEND}" --realm=${LDOMAIN}
      else
        samba-tool domain join ${LDOMAIN} DC -U"${URDOMAIN}\\${ADMIN_USER}" --password="${ADMIN_PASS}" --dns-backend="${DNS_BACKEND}" --site=${JOINSITE}
      fi
    else
      samba-tool domain provision --use-rfc2307 --domain=${URDOMAIN} --realm=${UDOMAIN} --server-role=dc --dns-backend="${DNS_BACKEND}" --adminpass="${ADMIN_PASS}" --host-ip=$HOST_IP

      if [[ ${NOCOMPLEXITY,,} == "true" ]]; then
        samba-tool domain passwordsettings set --complexity=off
        samba-tool domain passwordsettings set --history-length=0
        samba-tool domain passwordsettings set --min-pwd-age=0
        samba-tool domain passwordsettings set --max-pwd-age=0
      fi
    fi

    SMB_CONF_GLOBAL=(
      "wins support = yes"
      "winbind nss info = rfc2307"
      "rpc server dynamic port range = 49152-65535"
      # UID/GID mapping for local users
      # "idmap config * : backend = tdb"
      # "idmap config * : range = 10000-19999"
      # UID/GID mapping for domain users
      # "idmap config ${URDOMAIN} : backend = rid"
      # "idmap config ${URDOMAIN} : range = 1000000-1999999"
      # Template settings for users without ''unixHomeDir'' and ''loginShell'' attributes
      "template shell = /bin/bash"
      "template homedir = /home/%U"
    )

    if [[ ${INTERFACES} != "NONE" ]]; then
      SMB_CONF_GLOBAL+=("interfaces = ${INTERFACES}")
      SMB_CONF_GLOBAL+=("bind interfaces only = yes")
    fi

    if [[ ${DNS_BACKEND} == "SAMBA_INTERNAL" ]]; then
      SMB_CONF_GLOBAL+=("dns forwarder = ${DNSFORWARDER}")
    fi

    if [[ ${INSECURELDAP,,} == "true" ]]; then
      SMB_CONF_GLOBAL+=("ldap server require strong auth = no")
    fi

    for LINE in "${SMB_CONF_GLOBAL[@]}"; do
      sed -i "/\[global\]/a \\\t${LINE}" /etc/samba/smb.conf
    done

    cp -f /etc/samba/smb.conf /etc/external/smb.conf
  fi

  # setup kerberos
  if [[ -f "/etc/external/krb5.conf" ]]; then
    cp -f /etc/external/krb5.conf /etc/krb5.conf
  else
    cp -f /var/lib/samba/private/krb5.conf /etc/krb5.conf

    # nothing to do here

    cp -f /etc/krb5.conf /etc/external/krb5.conf
  fi

  # setup chronyd
  if [[ -f "/etc/external/chrony.conf" && -f "/etc/external/chrony.keys" ]]; then
    cp -f /etc/external/chrony.conf /etc/chrony.conf
    cp -f /etc/external/chrony.keys /etc/chrony.keys
  else
    cp -f /template/chrony.conf /etc/chrony.conf

    sed -i "s|xxx.xxx.xxx.xxx/xx|${HOST_NETWORK}|g" /etc/chrony.conf
    sed -i "s|xxx.xxx.xxx.xxx|${HOST_IP}|g" /etc/chrony.conf

    chronyc keygen > /etc/chrony.keys

    cp -f /etc/chrony.conf /etc/external/chrony.conf
    cp -f /etc/chrony.keys /etc/external/chrony.keys
  fi

  if [[ "${DNS_BACKEND}" == "BIND9_DLZ" ]]; then
    # setup bind
    if [[ -f "/etc/external/named.conf" ]]; then
      cp -f /etc/external/named.conf /etc/named.conf
    else
      cp -f /template/named.conf /etc/named.conf

      sed -i "s|xxx.xxx.xxx.xxx/xx|${HOST_NETWORK}|g" /etc/named.conf
      sed -i "s|xxx.xxx.xxx.xxx|${DNS_FORWARDER}|g" /etc/named.conf

      cp -f /etc/named.conf /etc/external/named.conf
    fi
  else
    # remove template for supervisor to ensure bind will not started

    rm -f /template/supervisor.d/named.conf
  fi;

  # setup supervisord
  if [[ -f "/etc/external/supervisord.conf" ]]; then
    cp -f /etc/external/supervisord.conf /etc/supervisord.conf
    cp -rf /etc/external/supervisor.d /etc
  else
    cp -f /template/supervisord.conf /etc/supervisord.conf
    cp -rf /template/supervisor.d /etc

    cp -f /etc/supervisord.conf /etc/external/supervisord.conf
    cp -rf /etc/supervisor.d /etc/external
  fi

  # overwrite resolv.conf
  {
    echo "search ${LDOMAIN}"
    echo "nameserver ${HOST_IP}"
  } > /etc/resolv.conf

  containerStart
}

containerStart() {
  if [[ -f "/etc/external/supervisord.conf" ]]; then
    # set permissions for chrony
    install -d /var/lib/samba/ntp_signd -g chrony -o root -m 0750

    # set permissions for named
    if [[ "${DNS_BACKEND}" == "BIND9_DLZ" ]]; then
      chgrp named /var/lib/samba/bind-dns/named.conf
      chmod g+r /var/lib/samba/bind-dns/named.conf
      chgrp named /var/lib/samba/private/dns.keytab
      chmod g+r /var/lib/samba/private/dns.keytab
      mkdir -p /var/named /var/log/named
      chgrp named /var/named /var/log/named
      chmod 770 /var/named /var/log/named
    fi

    # execute supervisord
    exec /usr/bin/supervisord -n -c /etc/supervisord.conf
  else
    printf "The configuration file /etc/external/supervisord.conf is missing! Try \"setup\" instead of \"start\"\n"
    exit 255
  fi
}

case "$1" in
start)
  containerStart
  ;;
setup)
  if [[ -f "/etc/supervisord.conf" ]]; then
    containerStart
  else
    containerSetup
  fi
  ;;
esac

exit 0
